//
//  TaskManagerViewController.swift
//  TaskManager
//
//  Created by Gary Lam on 01/05/15.
//  Copyright (c) 2015 Gary Lam. All rights reserved.
//

import UIKit
import CoreData

class TaskManagerViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    

    

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchedResultController = getFetchedResultController()
        fetchedResultController.delegate = self
        fetchedResultController.performFetch(nil)
    }
//********CoreData Handling
    //Create Database connection object
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    //Create result controller(0)
    var fetchedResultController: NSFetchedResultsController = NSFetchedResultsController()
    
    //Create query request(1)
    func contactFetchRequest() -> NSFetchRequest {
        let fetchRequest = NSFetchRequest(entityName: "Tasks")
        let sortDescriptor = NSSortDescriptor(key: "firstname", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        return fetchRequest
    }
    //Bind the query result to Fetched Result Controller(2)
    func getFetchedResultController() -> NSFetchedResultsController {
        fetchedResultController = NSFetchedResultsController(fetchRequest: contactFetchRequest(), managedObjectContext: managedObjectContext!, sectionNameKeyPath: nil, cacheName: nil)
        return fetchedResultController
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //Initialize number of section of the TableView
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        let numberOfSections = fetchedResultController.sections?.count
        return numberOfSections!
    }
    //Initialize number of rows of the section of the TableView
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRowsInSection = fetchedResultController.sections?[section].numberOfObjects
        return numberOfRowsInSection!
    }
//********TableView Handling (Data and layout)
    //Bind data to the cell of the TableView
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell
        let contact = fetchedResultController.objectAtIndexPath(indexPath) as! Tasks
        cell.textLabel?.text = contact.firstname
        //Show office number if mobile is not filled
        if (contact.mobile.isEmpty){
            cell.detailTextLabel?.text = contact.office
        }
        else{
            cell.detailTextLabel?.text = contact.mobile
        }
        //Change colour depends of gender
        if (contact.gender == 1){
            cell.textLabel?.textColor = UIColor(red: 255.0/255.0, green: 145/255.0, blue: 222/255, alpha: 1.0)
        }
        else {
            cell.textLabel?.textColor = UIColor(red: 48.0/255.0, green: 235/255.0, blue: 243/255, alpha: 1.0)
        }
        
        return cell
    }
    
    //Swipe left to delete the records from database
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        let managedObject:NSManagedObject = fetchedResultController.objectAtIndexPath(indexPath) as! NSManagedObject
        managedObjectContext?.deleteObject(managedObject)
        managedObjectContext?.save(nil)
    }
    
    //Passing data inside the cell to another View
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "edit" {
            let cell = sender as! UITableViewCell
            let indexPath = tableView.indexPathForCell(cell)
            let contactController:TaskDetailViewController = segue.destinationViewController as! TaskDetailViewController
            let contact:Tasks = fetchedResultController.objectAtIndexPath(indexPath!) as! Tasks
            contactController.contact = contact
        }
    }
    //Reload tableView data if data changed
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.reloadData()
    }
}
