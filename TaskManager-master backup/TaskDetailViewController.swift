//
//  TaskDetailViewController.swift
//  TaskManager
//
//  Created by Gary Lam on 01/05/15.
//  Copyright (c) 2015 Gary Lam. All rights reserved.
//

import UIKit
import CoreData

class TaskDetailViewController: UIViewController {

    //Create Database connection object
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    //Members and method initialzation
    @IBOutlet weak var txtfirstname: UITextField!
    @IBOutlet weak var txtlastname: UITextField!
    @IBOutlet weak var txtmobile: UITextField!
    @IBOutlet weak var txtoffice: UITextField!
    @IBOutlet weak var txtemail: UITextField!
    @IBOutlet weak var ismale: UISegmentedControl!
    @IBAction func changed(sender: AnyObject) {
        switch ismale.selectedSegmentIndex{
        case 0:
            print("changed to ")
            println("0")
            
        case 1:
            print("changed to ")
            println("1")
            
        default:
            println("OTHER VALUE")
        }
    }
    
    //Select appropriate action of Done button
    @IBAction func done(sender: AnyObject) {
        if contact != nil {
            editContact()
        } else {
            createContact()
        }
        dismissViewController()
    }
    
    //Action of Cancel button
    @IBAction func cancel(sender: AnyObject) {
        dismissViewController()
    }
    
    //Navigate back to last controller
    func dismissViewController() {
        navigationController?.popViewControllerAnimated(true)
    }
    
    var contact: Tasks? = nil
    
    override func viewDidLoad() {
        
        phcolor(txtfirstname, placeholdername: "firstname")
        phcolor(txtlastname, placeholdername: "lastname")
        phcolor(txtmobile, placeholdername: "mobile number")
        phcolor(txtoffice, placeholdername: "office number")
        phcolor(txtemail, placeholdername: "yourname@apple.com")
        super.viewDidLoad()
        if contact != nil {
            txtfirstname.text = contact?.firstname
            txtlastname.text = contact?.lastname
            txtmobile.text = contact?.mobile
            txtoffice.text = contact?.office
            txtemail.text = contact?.email
            ismale.selectedSegmentIndex = contact!.gender
            if(contact!.gender == 1){
                txtfirstname.textColor=(UIColor(red: 255.0/255.0, green: 145/255.0, blue: 222/255, alpha: 1.0))
                txtlastname.textColor=(UIColor(red: 255.0/255.0, green: 145/255.0, blue: 222/255, alpha: 1.0))
            }
            else{
                txtfirstname.textColor=(UIColor(red: 48.0/255.0, green: 235/255.0, blue: 243/255, alpha: 1.0))
                txtlastname.textColor=(UIColor(red: 48.0/255.0, green: 235/255.0, blue: 243/255, alpha: 1.0))
            }
            
            
        }
        
    }
    
    //Custom placeholder color
    func phcolor(uiname:UITextField,placeholdername:String){
        uiname.attributedPlaceholder =
NSAttributedString(string: placeholdername, attributes:[NSForegroundColorAttributeName : UIColor(red: 201.0/255.0, green: 161.0/255.0, blue: 75.0/255.0, alpha: 1.0)])
    }
   

    // Action of create new contact
    func createContact() {
        let entityDescripition = NSEntityDescription.entityForName("Tasks", inManagedObjectContext: managedObjectContext!)
        let contact = Tasks(entity: entityDescripition!, insertIntoManagedObjectContext: managedObjectContext)
        contact.firstname = txtfirstname.text
        contact.lastname = txtlastname.text
        contact.mobile = txtmobile.text
        contact.office = txtoffice.text
        contact.email = txtemail.text
        contact.gender = ismale.selectedSegmentIndex
        managedObjectContext?.save(nil)
    }
    //Action of edit existing contact
    func editContact() {
        contact?.firstname = txtfirstname.text
        contact?.lastname = txtlastname.text
        contact?.office = txtoffice.text
        contact?.mobile = txtmobile.text
        contact?.email = txtemail.text
        contact?.gender = ismale.selectedSegmentIndex
        managedObjectContext?.save(nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
