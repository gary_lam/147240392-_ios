//
//  Tasks.swift
//  TaskManager
//
//  Created by Gary Lam on 01/05/15.
//  Copyright (c) 2015 Gary Lam. All rights reserved.
//

import Foundation
import CoreData

class Tasks: NSManagedObject {

    @NSManaged var firstname: String
    @NSManaged var lastname: String
    @NSManaged var mobile: String
    @NSManaged var office: String
    @NSManaged var email: String
    @NSManaged var gender: Int
    
}
