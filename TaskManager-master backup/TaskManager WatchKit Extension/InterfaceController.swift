//
//  InterfaceController.swift
//  ToDoList WatchKit Extension
//
//  Created by Gary Lam on 23/5/15.
//  Copyright (c) 2015 No Name. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    @IBOutlet weak var btn_Refresh: WKInterfaceButton!
    
    @IBOutlet weak var name: WKInterfaceLabel!
    @IBOutlet weak var mphone: WKInterfaceLabel!
    @IBOutlet weak var ophone: WKInterfaceLabel!
    @IBOutlet weak var gender: WKInterfaceLabel!
    @IBOutlet weak var g_icon: WKInterfaceImage!
    
    @IBAction func refreshBtn() {
        refreshdata("ComeonBaby")
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        refreshdata("ComeonBaby")
        
        // Configure interface objects here.
    }
    //Get data from ParentApplication
    func refreshdata(messageText: String) {
        var messageText:String = ""
        let infoDictionary = ["message" : messageText]
        WKInterfaceController.openParentApplication(infoDictionary) {
            (replyDictionary, error) -> Void in
            
            if let castedResponseDictionary = replyDictionary as? [String: String],
                responseMessage = castedResponseDictionary["message"]
            {
                var contactsArray = split(responseMessage) {$0 == ","}
                var name: String = contactsArray[0]
                var mphone: String = contactsArray[1]
                var ophone: String = contactsArray[2]
                var gender: String = contactsArray[3]
                self.name.setText(" " + name)
                self.mphone.setText(" " + mphone)
                self.ophone.setText(" " + ophone)
                self.gender.setText(" " + gender)
                if(gender == "Female"){
                    self.gender.setTextColor(UIColor(red: 255.0/255.0, green: 145/255.0, blue: 222/255, alpha: 1.0));
                    self.g_icon.setImageNamed("icon_female.png")
                }
                else{
                    self.gender.setTextColor(UIColor(red: 48.0/255.0, green: 235/255.0, blue: 243/255, alpha: 1.0));
                    self.g_icon.setImageNamed("icon_male.png")
                    
                }
            }
        }
    }
    


    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
